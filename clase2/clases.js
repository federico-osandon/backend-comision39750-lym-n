// function Persona(){} // función constructora
class Persona {
    constructor(nombre, apellido, email){
        this.nombre = nombre
        this.apellido = apellido
        this.email = email
    }

    static variablesEstaticas = 'hola'
    saludar(){
        return 'hola esto soy una persona'
    }

    getNonbreCompleto(){
        return `${this.nombre}  ${this.apellido}`
    }

} 



const persona = new Persona('Fede', 'Osandon', 'f@gmail.com')
console.log(persona.nombre)
console.log(persona.apellido)
console.log(persona.email)
console.log(persona.saludar())
console.log(persona.getNonbreCompleto())


const persona2 = new Persona('Juan', 'Perez', 'j@gmail.com')
console.log(persona2.nombre)


console.log(Persona.variablesEstaticas)
console.log(Date.now())
// Array.length
