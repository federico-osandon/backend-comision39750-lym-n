// ej let


// let i  = 0 // global

// function foo() {
//     i=1 // i global
//     let j = 2 // local a foo
//     if (true) {
//         console.log(i)
//         console.log(j)
//     }
// }
// foo()

// function foo1() {
//     let i = 0 // local a foo1 1
    
//     if (true) {
//         let i = 1 // 2 
//         console.log(i) // 2
//     }
//     console.log(i) // 1
// }
// foo1()



// function foo2() {
//     if (true) {
//         let i = 1        
//     }
//     console.log(i)
// }
// foo2()

// const 

// const i = 0
// // TypeError: Assignment to constant variable.
// // i = 1

// const obj = {
//     i : 0
// }
// obj.i++
// const objPersona = {
//     nombre : 'Pepe'
// }
// objPersona.nombre= 'Juan'
// console.log(objPersona)

// // funciones 

// // const foo = function() {}
// const foo = () => 'hola' // this 


// const nombreDeLaFuncion = function  (params){
//     // en el cuarpo de la función están las instrucciones que queremos que realice
//     let saludo = 'Hola como están'
//     // mandamos para afuera la varible porque el valor muere. 
//     return saludo
// }
// const nombreDeLaFuncion = (params) => {
//     // en el cuarpo de la función están las instrucciones que queremos que realice
//     let saludo = 'Hola como están'
//     // mandamos para afuera la varible porque el valor muere. 
//     return saludo
// }


// const identificadorDeLaFuncion = _ =>  'Hola como están'

// const sumarDosNumeros = (numero1, numero2) => {
//     return numero1 + numero2
// }

// console.log(identificadorDeLaFuncion('y fede es el mejor'))
// console.log(sumarDosNumeros(2, 3))
// console.log(nombreDeLaFuncion('hola')) // 256


// Scope
function nombreDeLaFuncion (params){
    // en el cuarpo de la función están las instrucciones que queremos que realice
    // mandamos para afuera la varible porque el valor muere. 
    let saludo = `Hola como están ${params}. Todo bien?`
    return saludo
}

console.log(nombreDeLaFuncion('Bienvenidos a la clase de JS'))
