const { Router } = require('express')

const router = Router()
const nombres = ['fede', 'juan']

router.param('nombre', (req, res, next, nombre)=>{
    if(!nombres.includes(nombre)) {
        req.nombre = null
    }else{
        req.nombre = nombre
    }
    next()
})

/// siempre string -> string (email:'@ .', nombre: letras ) $@/&% un gran problema
// á  -> %C3%A1
router.get('/params/:nombre([a-z%C3%A1%C3%A9%C3%AD%C3%B3%C3%BA%C3%BC]+)', (req,res) => {
    res.send({
        message: req.nombre
    })
})
router.get('/params/:nombre([a-z%C3%A1%C3%A9%C3%AD%C3%B3%C3%BA%C3%BC]+)/:apellido', (req,res) => {
    res.send({
        message: req.params.nombre
    })
})


router.put('/params/:nombre([a-z%C3%A1%C3%A9%C3%AD%C3%B3%C3%BA%C3%BC]+)', (req,res) => {
    res.send({
        message: req.params.nombre
    })
})
router.delete('/params/:nombre([a-z%C3%A1%C3%A9%C3%AD%C3%B3%C3%BA%C3%BC]+)', (req,res) => {
    res.send({
        message: req.params.nombre
    })
})

router.get('*', async (req,res)=> {
    res.status(404).send('404 Not found')
})


module.exports = router