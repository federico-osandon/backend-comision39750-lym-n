const { Router } = require('express')
const jwt = require('jsonwebtoken')
// const router = Router()

// router.get()
// router.post()
// module.exports = router

class RouterClass {
    constructor(){
        this.router = Router()
        this.init()
    }

    getRouter(){
        return this.router
    }

    init(){}

    // [ (req,res,next)=>{} ] 
    applyCallbacks(callbacks){
        return callbacks.map(callback => async(...params)=>{
            try {
                // apply, ejecutará la función callback apuntando directamente a una
                // instancia de la clase, por ello, colocamos this para que utilice 
                // solo el contexto de este router, los parámetros son internos
                // de cada callback, sabemos que los params de un callback corresponden a 
                // req, res, next
                await callback.apply(this, params)
                
            } catch (error) {
                console.log(error)
                params[1].status(500).send(error)
            }
        })

    }

    generateCustomResponse = (req, res, next)=>{
        res.sendSuccess = payload => res.send({status: 'success', payload})
        res.sendServerError = error => res.send({status: 'error', error})
        res.sendUserError = error => res.send({status: 'error', error})
        next()
    }

    handlePolicies = policies => (req, res, next)=>{
        if (policies[0]==='PUBLIC')  return next()
        const authHeader = req.headers.authorization
        if (!authHeader) return res.send({status: 'error', error: 'no autorizado'})
        // [Beares, fahjsdfkshdaf]
        const token  = authHeader.split(" ")[1]
        const user = jwt.verify(token, 'CoderSecreto')
        if(!policies.includes(user.role.toUpperCase())) return res.status(403).send({status: 'success', error: 'not permissions'})
        req.user = user
        next()
    }

    // router.get('/', (req, res)=>{})
    get(path, policies,...callbacks){ //[(req,res)=>{}]
        this.router.get(path, this.handlePolicies(policies),this.generateCustomResponse,this.applyCallbacks(callbacks))
    }
    post(path, policies,...callbacks){ //[(req,res)=>{}]
        this.router.post(path, this.handlePolicies(policies),this.generateCustomResponse,this.applyCallbacks(callbacks))
    }
    put(path, policies,...callbacks){ //[(req,res)=>{}]
        this.router.put(path, this.handlePolicies(policies),this.generateCustomResponse,this.applyCallbacks(callbacks))
    }
    delete(path, policies,...callbacks){ //[(req,res)=>{}]
        this.router.delete(path, this.handlePolicies(policies),this.generateCustomResponse,this.applyCallbacks(callbacks))
    }
    
    
}

module.exports = RouterClass

