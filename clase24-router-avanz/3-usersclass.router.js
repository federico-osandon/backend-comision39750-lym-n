// const {Router} = require('express')
const RouterClass = require('./2-RouterClass.js')

class UserRouter extends RouterClass{
    init(){
        this.get('/',['PUBLIC'],async(req,res)=>{
            try {
                res.sendSuccess('hola coder')
                
            } catch (error) {
                res.sendServerError(error)
            }
        })
        this.get('/current',['ADMIN'],async(req,res)=>{
            try {
                res.sendSuccess('validar')
                
            } catch (error) {
                res.sendServerError(error)
            }
        })
    }
}

module.exports = UserRouter


// function(...arg){} -> [arg1, arg2,arg3]

// function(arg1, arg2, arg3)