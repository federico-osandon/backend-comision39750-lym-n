// const express = require('express')
const express = require('express')
//________________________________________________________________
const UserRouter = require('./routes/newUser.router.js')

const app = express()
const usersRouter = new UserRouter()

const PORT = process.env.PORT || 8080



app.use('/api/usuarios',  usersRouter.getRouter())


const httpServer = app.listen(PORT,err =>{
    if (err)  console.log(err)
    console.log(`Escuchando en el puerto: ${PORT}`)
})

