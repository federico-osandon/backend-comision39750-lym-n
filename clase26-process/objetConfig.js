const {connect} = require('mongoose')
const dotenv = require('dotenv')
const { commander } = require('../utils/commander')

const { mode } = commander.opts()
dotenv.config({
    path: mode === 'development' ? './.env.development': './.env.production' 
})


// let url = 'mongodb+srv://federico:federico1@cluster0.tzkuy8w.mongodb.net/comsion39750?retryWrites=true&w=majority'
let url = process.env.MONGO_URL_LOCAL


module.exports = {
    port: process.env.PORT,
    jwt_secret_key: process.env.JWT_SECRET_KEY,
    connectDB: async () => {
        try {
            connect(url)
            console.log('Base de datos conectadas')             
        } catch (err) {
            console.log(err)
        } 
    }
}

