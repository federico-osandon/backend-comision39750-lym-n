const {Router} = require('express')
const { auth } = require('../middlewares/autenticacion.middleware')
const {fork} = require('child_process')

const router = Router()

function operacionCompleja() {
    let result = 0
    for (let i = 0; i < 9e9; i++) {
        result += i        
    }
    return result
}


router.get('/block', (req, res)=>{
    const result = operacionCompleja()
    res.send(`el resultado de la operación es ${result}`)
})

router.get('/noblock', (req, res)=>{
    const child = fork('./src/routes/operacionCompleja.js')
    
    child.send('Inicia el processo de cáclculo')
    child.on('message', result => {
        res.send(`el resultado de la operación es ${result}`)
    })
})

router.get('/suma', (req, res)=>{    
    res.send(`HNola mundo`)
})






module.exports = router