const UserDaoMongo    = require("../Daos/Mongo/user.mongo");
const ProductDaoMongo = require('../Daos/Mongo/product.mongo.js')

const userService = new UserDaoMongo()
const productService = new ProductDaoMongo()

module.exports = {
    userService,
    productService
}