const config = require('../config/objetConfig.js')
let UserDao
let ProductDao
let ContactDao

switch (config.persistence) { // mongo - archivos - memoria
    case 'MONGO':
         // mongo conexion
        config.connectDB()
        const ProductDaoMongo = require('../Daos/Mongo/product.mongo.js')
        const UserDaoMongo = require('../Daos/Mongo/user.mongo.js')
        const ContactDaoMongo = require('../Daos/Mongo/contact.mongo.js')
        
        UserDao    = UserDaoMongo
        ProductDao = ProductDaoMongo
        ContactDao = ContactDaoMongo
        break;
    case 'FILE':
        const ProductDaoFile = require('../Daos/file/product.file.js')
        const UserDaoFile = require('../Daos/file/userManager.js')
        
        UserDao    = UserDaoFile
        ProductDao = ProductDaoFile
        break;
    case 'MEMORY':
        const ProductDaoMemory = require('../Daos/Memoria/product.memory.js')
        const UserDaoMemory = require('../Daos/Memoria/users.memory.js')
        
        UserDao    = UserDaoMemory
        ProductDao = ProductDaoMemory
        break;

    default:
        break;
}

module.exports = {
    UserDao,
    ProductDao,
    ContactDao
}

