const { 
    ContactDao, 
    ProductDao, 
    UserDao 
} = require("../Daos/factory")
const ContactRepository = require("../repositories/contacts.repository")

const productService = new ProductDao()
const userService = new UserDao()

const contactService = new ContactRepository(new ContactDao())

module.exports = {
    productService,
    userService,
    contactService
}