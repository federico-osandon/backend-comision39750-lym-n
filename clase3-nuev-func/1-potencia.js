const valores = [1,2,3,4,5,6,7,8,9,10]

// const potencia = valores.map(function(valor){ return valor ** valor })
const potencia = valores.map((valor, index) => valor ** index ) 

// console.log(potencia)
// a 0 = 1
// a 1 = a 

const nombres = ['Juan', 'Pedro', 'Maria', 'Jose', 'Luis', 'Ana']

console.log(nombres.includes('Juan'))