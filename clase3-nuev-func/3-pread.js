const obj1 = {
    propiedad1: 1,
    propiedad2: 'b',
    propiedad3:  true
}

const obj2 = {
    propiedad1: 'c',
    propiedad2: [1, 2, 3],
    propiedad4:  false
}



// destructuring
// const propiedad1 =  obj1.propiedad1
// const propiedad2 =  obj1.propiedad2
// const propiedad3 =  obj1.propiedad3

// const { propiedad1 = propiedad1 } = obj1
// const { propiedad1,propiedad2  } = obj1

const obj3 = { ...obj1, ...obj2 }

let {propiedad1, ...propiedadesQueRestan} = obj2

console.log(resto)