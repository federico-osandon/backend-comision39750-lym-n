const modo = 'no'

const ejemploImport = async () => {
    if(modo === 'calculos'){
        // import { Calculadora } from './calculos.js'
        const { Calculadora } = await import('./calculadora.js')
        const calculadora = new Calculadora()
        console.log(calculadora.suma(1,2))
    }
}

console.log('HGola')

ejemploImport()