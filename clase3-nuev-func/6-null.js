// let variablePrueba = 1

// let prueba1 = variablePrueba || 'sin Valor'

// let prueba2 = variablePrueba??'sin Valor'

// console.log(prueba1)
// console.log(prueba2)

// js 0 es booleano

class Persona {
    // #name // -> propiedad privada

    constructor(name, age) {
        this.name = name
        this.age = age
        this.#fullname = `${name} ${age}`

    }

    getName() {
        return this.name
    }

    getFullName() {
        return this.#fullname
    }

    #metodoPrivado = () =>  `Solo accesible para la clase`
}

const persona = new Persona('John', 30)

// console.log(persona.())