const mongoose = require('mongoose')
let ProductDao

switch ('mongo') {
    case 'mongo':
        // conección
        mongoose.connect('mongodb://localhost:27017/arquitectura')
        console.log('db conectada')

        const ProductDaoMongo = require('./mongo/product.mongo.js')

        ProductDao = ProductDaoMongo
        break;

    default:
        break;
}

module.exports = {
    ProductDao
}