const {Router}   = require('express')

const compression = require('express-compression')

const router = Router()

// router.use(compression()) gzip
// brotli
router.use(compression({
    brotli: {
        enabled: true,
        zlib: {}
    }
}))

router.get('/stringmuylargo', (req, res)=> {
    let string = `Hola Coders, soy una string ridículamente largo`
    for(let i=0; i<5e4; i++){
        string += `Hola Coders, soy una string ridículamente largo`
    }
    res.send(string)
})

module.exports = router