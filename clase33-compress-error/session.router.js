const {Router} = require('express')
// error
const { CustomError } = require('../utils/CustomError/CustomError')
const { generateUserErrorInfo } = require('../utils/CustomError/info')
const { EError } = require('../utils/CustomError/EErrors')

const router = Router()


router.post('/register', async (req, res, next) => {
    try {
        const {first_name, last_name, email} = req.body 
        
        //validar si vienen distintos de vacios && caracteres especiales
        if (!first_name || !last_name || !email) {
            CustomError.createError({
                name: 'User creation error',
                cause: generateUserErrorInfo({
                    first_name, 
                    last_name,
                    email
                }),
                message: 'Error trying to created user',
                code: EError.INVALID_TYPE_ERROR
            })
        }
    
       
    
        let token = generateToken({
            first_name: 'fede',
            last_name: 'Osandon',
            email: 'f@gmail.com'
        })
    
    
        res.status(200).send({
            status: 'success',
            message: 'Usuario creado correctamente',
            token
        })
    } catch (error) {
        next(error)
    }
   
})



module.exports = router