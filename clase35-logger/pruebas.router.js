const {Router}   = require('express')
const { auth }   = require('../middlewares/autenticacion.middleware')
const {fork}     = require('child_process')
const { dirname } = require('path')
const { sendMail } = require('../utils/sendmail')
const { sendSms, sendWhatsapp } = require('../utils/sendSms')
const {generateUser} = require('../utils/generateUserFaker.js')

const compression = require('express-compression')
const { logger } = require('../config/logger')
const { faker } = require('@faker-js/faker')

const router = Router()

router.get('/testuser', (req, res)=>{
    let persona = {
        first_name: faker.person.firstName(),
        last_name:  faker.person.lastName(),
        email:      faker.internet.email(),
        password:   faker.internet.password()
    }

    res.send({
        status: 'success', 
        payload: persona
    })
})

router.get('/simple',(req,res) => {
    let suma = 0
    for (let i = 0; i < 1000000; i++) {
        suma += i
    }
    res.send({suma})
})

router.get('/compleja',(req,res) => {
    let suma = 0
    for (let i = 0; i < 5e8; i++) {
        suma += i
    }
    res.send({suma})
})
// npm i winston
// npm i -g artillery
// artillery quick --count 40 --num 50 'http://localhost:8080/pruebas/simple' -o simple.json 
// artillery quick --count 40 --num 50 'http://localhost:8080/pruebas/compleja' -o compleja.json 
// npm i artillery-plugin-metrics-by-endpoint
// artillery run config.yaml --output testPerformance.json


router.get('/logger', (req,res)=> {
    // req.logger.warn('alerta')
    // req.logger.error('alerta')
    // req.logger.info('info')
    // req.logger.warning('warning')
    // req.logger.warning('warning')
    // req.logger.error('error')
    // req.logger.fatal('fatal error')
    // logger.warning('warning')
    res.send({message: 'Prueba de logger'})
})




module.exports = router