const express = require('express')
const session = require('express-session')
const cookieParser = require('cookie-parser')

const objectConfig = require('./config/objetConfig.js')
const appRouter = require('./routes/index.js')

// ______________________________________________________


const FileStore  = require('session-file-store')
const {create} = require('connect-mongo') 

// hbs __________________________________________________________________
const handlebars = require('express-handlebars')
const { connect } = require('mongoose')

// passport 
// const { initPassport, initPassortGithub } = require('./config/passport.config.js')
const passport = require('passport')
const { initPassport } = require('./passport-jwt/passport.config.js')

// const { suma } = require('sumac39750')

//__________________________________________________________________________+
const { Server: ServerHTTP } = require('http')
const { Server: ServerIO } = require('socket.io')

const cors = require('cors')
const { errorHandler } = require('./middlewares/error.middleware.js')
const { addLogger, logger } = require('./config/logger.js')

const app = express()
const serverHttp = new ServerHTTP(app)
const io         = new ServerIO(serverHttp)
const PORT =  process.env.PORT




// const io = new Server(httpServer)


app.engine('handlebars', handlebars.engine())
app.set('views', __dirname+'/views')
app.set('view engine', 'handlebars')
// hbs __________________________________________


app.use(express.json()) 
app.use(cors()) 
app.use(express.urlencoded({extended: true}))
// console.log(__dirname+'/public')
app.use('/static', express.static(__dirname+'/public'))
app.use(cookieParser('P@l@braS3cr3t0'))
app.use(addLogger)


initPassport()
passport.use(passport.initialize())
// passport.use(passport.session())

app.use(appRouter)

app.use(errorHandler)
// app.use((err, req, res, next)=>{
//     console.log(err)
//     res.status(500).send('Todo mal')
// })

// console.log(suma('2'))

const socketMessage = (io) => {
    let messages = []
    io.on('connection', socket => {
        console.log('Nuevo cliente conectado')
        socket.on('message', data => {
            // console.log(data)
            messages.push(data)
            io.emit('messageLogs', messages)
        })
    
        socket.on('authenticated', data => {
            socket.broadcast.emit('newUserConnected', data)
        })
    
    })

}

socketMessage(io)

exports.initServer = () => serverHttp.listen(PORT,()=>{
    logger.info(`Escuchando en el puerto: ${PORT}`)
})




