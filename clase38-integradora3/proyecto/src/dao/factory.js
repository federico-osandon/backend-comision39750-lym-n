const { config } = require('../config/configServer.js')
let ProductDao 
let UserDao 

switch ('mongo') {
    case 'mongo':
        config.connectDb()
        ProductDao = require("./mongo/product.mongo.js") 
        break;

    default:
        break;
}

module.exports = {
    ProductDao
}
