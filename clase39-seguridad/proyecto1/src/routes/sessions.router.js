import { Router } from 'express';

const router = Router();

const users = [];

router.post('/register',(req,res)=>{
    const user = req.body; // datos cualquier cosa, no haber nada
    console.log(user);
    // A04 Insecure Design
    if(users.length===0) user.id = 1;
    else user.id = users[users.length-1].id+1;
    // Hash de password ? 
    users.push(user);
    // devuelve todos los datos
    res.send({status:"success",payload:user})
})

export default router;