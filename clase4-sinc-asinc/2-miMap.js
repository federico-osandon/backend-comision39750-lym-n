let arregloPrueba = [1,2,3,4,5]

function myMap(arreglo, callback) {
    let nuevoArreglo = [
        //2,4,6,8,10
    ]

    for (let i=0 ; i < arreglo.length; i++) {
      let nuevoValor = callback(arreglo[i])

      nuevoArreglo.push( nuevoValor )
    }

    return nuevoArreglo
}

let nuevoArregloPropio = myMap(arregloPrueba, (x) => { return x * 2})

console.log(nuevoArregloPropio)
Array.prototype.miPropioMap = function(callback){
    var nuevoArreglo = []
    for (let i = 0; i < this.length; i++) {
      let nuevoValor = callback(this[i])
      nuevoArreglo.push(nuevoValor);
    }
    return nuevoArreglo;
  }
  arregloPrueba.miPropioMap(x=> x*2)

console.log(arregloPrueba.miPropioMap(x=> x*2))