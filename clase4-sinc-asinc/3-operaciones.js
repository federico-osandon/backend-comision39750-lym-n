const suma = (num1, num2)=> num1 + num2 
const resta = (num1, num2)=> num1 - num2 
const mult = ( num1, num2)=> num1 * num2 
const div = (err, num1, num2)=> err  ? 'No se puede divir por 0'  : num1 / num2 

const operaciones = (numero1, numero2, cb) => {
    console.log('Bienvenido a la calculadora')
    // console.log(numero1)
    // console.log(numero2)
    if (numero2 === 0) {
        return cb(true, numero1, numero2)
    }

    return cb(null, numero1, numero2)
}
// console.log(operaciones( 2,3, suma))
// console.log(operaciones( 2,3, resta))
// console.log(operaciones( 2,3, mult))
console.log(operaciones( 2,0, div))