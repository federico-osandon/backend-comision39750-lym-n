const dividir = (dividendo, divisor) => {

    return new Promise( ( resuelto, rechazado )=>{ // o res, rej - resulto o rechazado
        setTimeout(()=>{ // async
            if(divisor === 0){
                rechazado( 'no se puede dividi por 0' )
    
            }else{
                resuelto( dividendo/divisor )
            }
        },3000)
        
    })
}

// dividir(1,1)
//     .then( resultado => {
//         console.log(resultado)
        
//         return resultado*2
//     } )
//     .then( resultado => resultado*2)
//     .then( resultado => console.log(resultado) 
//         // desde acá puedo obtener el resultado del then uno
//     )
//     .catch( err => console.log(err) )
//     .finally( () => console.log('Finalmente') )
    
// async / await
let resultadoGlobal = 
async function funcionAsync(){
    try {
        let resultado = await dividir(1,1)
        resultado  += 5
        resultadoGlobal =  resultado ** 2
    } catch (error) {
        console.log(error)
    }
}

funcionAsync()
console.log(resultadoGlobal)


