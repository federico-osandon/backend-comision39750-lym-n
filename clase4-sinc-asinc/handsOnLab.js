const suma = (numero1, numero2) => {
    return new Promise((resolve, reject) => {
        if(numero1 === 0 || numero2 === 0) reject('Operación no válida')
        if(numero1+numero2 < 0) reject('La calculadora no puede calcular números negativos')
        resolve(numero1+numero2)
    })
}
const resta = (numero1, numero2) => {
    return new Promise((resolve, reject) => {
        if(numero1 === 0 || numero2 === 0) reject('Operación no válida')
        if(numero1- numero2 < 0) reject('La calculadora no puede calcular números negativos')
        resolve(numero1-numero2)
    })
}

const calculo = async () => {
    try {
        let numero1= 5
        let numero2= 5
        let resultado = await suma(numero1, numero2)
        let resultadoResta = await resta(numero1, numero2)
        console.log(resultado)
        console.log(resultadoResta)
    } catch (error) {
        console.log(error)
    }
}
calculo()