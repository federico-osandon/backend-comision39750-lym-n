import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ProudctsModule } from './proudcts/proudcts.module';

@Module({
  imports: [UsersModule, ProudctsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
