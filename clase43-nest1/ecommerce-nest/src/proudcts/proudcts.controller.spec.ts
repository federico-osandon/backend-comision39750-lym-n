import { Test, TestingModule } from '@nestjs/testing';
import { ProudctsController } from './proudcts.controller';
import { ProudctsService } from './proudcts.service';

describe('ProudctsController', () => {
  let controller: ProudctsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProudctsController],
      providers: [ProudctsService],
    }).compile();

    controller = module.get<ProudctsController>(ProudctsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
