import { Module } from '@nestjs/common';
import { ProudctsService } from './proudcts.service';
import { ProudctsController } from './proudcts.controller';

@Module({
  controllers: [ProudctsController],
  providers: [ProudctsService]
})
export class ProudctsModule {}
