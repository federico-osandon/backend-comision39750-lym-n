import { Controller, Get, Post, Body, Patch, Param, Delete, HttpException, HttpStatus, Query, Request } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Controller('users') // ruter/controller
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: User) {
    if (!createUserDto.first_name || !createUserDto.email || !createUserDto.password ) throw new HttpException('datos incompletos', HttpStatus.BAD_REQUEST)
    return this.usersService.create(createUserDto);
  }

  @Post('/:b')
  probarRequest(@Request() req){
    const {query, body, params} = req
    console.log(query)
    console.log(params)
    console.log(body)
    return 'todo en un objeto'
  }

  // @Get()
  // findAll(@Query() query) {
  //   const { limit } = query
  //   let users = this.usersService.findAll();
  //   return {status: 'success', users, limit}
  // }
  @Get()
  findAll(@Query('limit') limit) {    
    let users = this.usersService.findAll();
    return {status: 'success', users, limit}
  }
  // res.params - res.query - res.body
  @Get(':id')
  findOne(@Param('id') id: string) {
    if (isNaN(+id)) {
        throw new HttpException('Parámetro inválido', HttpStatus.BAD_REQUEST)
    }
    return this.usersService.findOne(+id);
  }

  @Patch(':id') // put: actualiza todo  - patch: solo una parte
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
