
// const fs = require('fs')
// // console.log(fs)

// // fs.writeFileSync('./data.txt', 'Hola Coders! \n', 'utf-8')
// console.log(fs.existsSync('./data.txt'))

// fs.appendFileSync('./data.txt', 'ESto es un agregado \n', 'utf-8')

// if (fs.existsSync) {
//     const contenidoArchivo = fs.readFileSync('./data.txt', 'utf-8')
//     console.log(contenidoArchivo)
// }

// fs.unlinkSync('./data.txt')



// con callbacks ___________________________________________________________________

// const fs = require('fs')
// let contenidoTexto = ''

// let texto = 'Este es el contenido'
// // fs.writeFile('./data.txt', texto, 'utf-8', (err)=>{
// //     if(err) console.log('ha ocurrido un error')
// // })

// fs.appendFile('./data.txt', 'agregado ', 'utf-8', (err)=>{
//     if (err) {
//         return err
//     }
//     console.log('Agregado listo')
// })

// fs.readFile('./data.txt', 'utf-8',(err, contenido)=>{
//         if(err) console.log('ha ocurrido un error')
//         console.log(contenido)
//     })
    
//     fs.unlink('./data.txt', (err) =>{
//         if(err) console.log('ha ocurrido un error')
//         console.log('eliminado')

// })

// console.log(contenidoTexto)



// promesas fs ______________________________________________________________________
// const fs = require('fs')
const { promises } = require('fs')
const fs = promises

// fs.promises.writeFile

// fs.writeFile('ruta', 'texto', 'utf-8', cb)
const operacionesAsyncronicas = async ()=>{
    try {
        // await fs.writeFile('./data.txt', 'texto nuevo\n', 'utf-8')
        // .then(() => console.log('Terminó de escribir el archivo'))
        // .catch(err => console.log(err))
        
        // await fs.appendFile('./package.json', 'esto es un agregado ñ\n', 'utf-8')
        
        let contenido = await fs.readFile('./package.json', 'utf-8')
        // convertir a objeto javascript
        const respuestaParseada= JSON.parse(contenido)
        respuestaParseada.apellido = 'Fede el mejor'
        respuestaParseada.size = contenido.length
        // convertir a objeto Json
        const respParseadaJson = JSON.stringify(respuestaParseada, 'null', 2)
        console.log(respParseadaJson)
        
        await fs.writeFile('./info.json', respParseadaJson, 'utf-8')

        // await fs.unlink('./data.txt')
    } catch (error) {
        console.log(error)
    }

}

operacionesAsyncronicas()


