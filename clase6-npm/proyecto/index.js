const moment = require('moment')

console.log(moment())

let fechaActual = moment()
let nacimiento = moment('1990-10-10', 'YYYY')
if (fechaActual.isValid() && nacimiento.isValid()) {
    let diferencia  = fechaActual.diff(nacimiento, 'days')
    console.log(diferencia)
}