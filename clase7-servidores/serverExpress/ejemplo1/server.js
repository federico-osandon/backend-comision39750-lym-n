const express = require('express')
// import express from 'express'

const app = express()

let usuarios = [
    {id: '1', nombre: 'Fede 1', apellido:'Osandon 1'},
    {id: '2', nombre: 'Fede 2', apellido:'Osandon 2'},
    {id: '3', nombre: 'Fede 3', apellido:'Osandon 3'}
]

app.use(express.urlencoded({extended: true}))

// GET http:// localhost:8080 /
app.get('/query', (request, response)=>{
    console.log(request.query)
    const {nombre, apellido} = request.query
    response.send({nombre, apellido})
})


app.listen(8080, ()=>{
    console.log('Escuchando en el puerto 8080')
})

