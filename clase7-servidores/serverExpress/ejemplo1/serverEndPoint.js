const express = require('express')
// import express from 'express'

const app = express()

let usuarios = [
    {id: '1', nombre: 'Fede 1', apellido:'Osandon 1'},
    {id: '2', nombre: 'Fede 2', apellido:'Osandon 2'},
    {id: '3', nombre: 'Fede 3', apellido:'Osandon 3'},
]

// GET http:// localhost:8080 /
app.get('/bienvenida', (request, response)=>{
    // console.log(request)
    response.send('<h1 style="color: blue;">Hola coders</h1>')
})
// GET http:// localhost:8080 /usuario
app.get('/usuario/', (request, response)=>{
    // console.log(request)
    response.send({nombre: 'Fede', apllido: 'Osandon', correo: 'f@gmail.com'})
})

// GET http:// localhost:8080 /usuario/algomas
app.get('/usuario/:nombre', (request, response)=>{
    console.log(request.params)
    // console.log(request)
    response.send({nombre: request.params.nombre, apllido: 'Osandon', correo: 'f@gmail.com'})
})

// GET http:// localhost:8080 /usuario/nombre/apllido
app.get('/usuario/:nombre/:apellido', (request, response)=>{
    console.log(request.params)
    // console.log(request)
    response.send({nombre: request.params.nombre, apllido: request.params.apellido, correo: 'f@gmail.com'})
})

app.listen(8080, ()=>{
    console.log('Escuchando en el puerto 8080')
})

