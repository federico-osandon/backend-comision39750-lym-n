const express = require('express')
// import express from 'express'

const app = express()

let usuarios = [
    {id: '1', nombre: 'Fede 1', apellido:'Osandon 1'},
    {id: '2', nombre: 'Fede 2', apellido:'Osandon 2'},
    {id: '3', nombre: 'Fede 3', apellido:'Osandon 3'}
]

app.use(express.urlencoded({extended: true}))

// GET http:// localhost:8080 /
app.get('/', (request, response)=>{
    // console.log(request)
    response.send({usuarios})
})
// GET http:// localhost:8080 /usuario
app.get('/:idUsuario', (request, response)=>{
    // console.log(request)
    const {idUsuario } = request.params
    const usuario = usuarios.find(user => user.id === idUsuario)
    if(!usuario) return response.send({error: 'No se encuentra el usuario'})
    response.send({usuario})
})


app.listen(8080, ()=>{
    console.log('Escuchando en el puerto 8080')
})

