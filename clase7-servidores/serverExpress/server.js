const express = require('express')
// import express from 'express'

const app = express()
const usuarios = [
    { id: '1', nombre: 'nombre 1', apellido: 'apellido 1', genero: 'F' },
    { id: '2', nombre: 'nombre 2', apellido: 'apellido 2', genero: 'F' },
    { id: '3', nombre: 'nombre 3', apellido: 'apellido 3', genero: 'M' },
    { id: '4', nombre: 'nombre 4', apellido: 'apellido 4', genero: 'F' },
    { id: '5', nombre: 'nombre 5', apellido: 'apellido 5', genero: 'M' },
    { id: '6', nombre: 'nombre 6', apellido: 'apellido 6', genero: 'M' },
    { id: '7', nombre: 'nombre 7', apellido: 'apellido 7', genero: 'F' },
    { id: '8', nombre: 'nombre 8', apellido: 'apellido 8', genero: 'M' }
]

app.use(express.urlencoded({extended: true}))

// GET http:// localhost:8080 /
app.get('/usuarios', (request, response)=>{
    
    const { genero } = request.query

    if (!genero || (genero!=='M'&&genero!=='F')) {
        return response.send({usuarios})
    }

    let userFilter = usuarios.filter(user => user.genero === genero)

    response.send({usuarios})
})


app.listen(8080, ()=>{
    console.log('Escuchando en el puerto 8080')
})

